let reservations_menu = $("#batch_connect_session_context_slurm_reservation");
let accounts_menu = $("#batch_connect_session_context_slurm_account");
let partitions_menu = $("#batch_connect_session_context_slurm_partition");
let gpu_count = $("#batch_connect_session_context_slurm_gpu_count");
let gpus_menu = $("#batch_connect_session_context_slurm_gpu");

function update_accounts() {
    let reservation = reservations_menu.val();
    let account = accounts_menu.val();
    
    $("#batch_connect_session_context_slurm_account > option").each(function() {
        if(reservation === '' || ($(this).data('reservations') && $(this).data('reservations').includes(reservation))) {
            $(this).removeAttr('disabled');
        } else {
            $(this).attr('disabled','disabled');
            if($(this).val() == account) {
                $(this).prop('selected', false);
            }
        }
    });
    
    update_partitions();
}

function update_partitions() {
    let account = accounts_menu.val();
    let reservation = reservations_menu.val();
    let partition = partitions_menu.val();
    
    $("#batch_connect_session_context_slurm_partition > option").each(function() {
        if($(this).data('accounts').includes(account) && (reservation === '' || $(this).data('reservations').includes(reservation))) {
            $(this).removeAttr('disabled');
        }
        else
        {
            $(this).attr('disabled','disabled');
            if($(this).val() == partition) {
                $(this).prop('selected', false);
            }
        }
    });
    
    update_gpus();
}

function update_gpus() {
    let partition = partitions_menu.val();
    let gpu = gpus_menu.val();
    
    $("#batch_connect_session_context_slurm_gpu > option").each(function() {
        if($(this).val() === '' || $(this).data('partitions').includes(partition)) {
            $(this).removeAttr('disabled');
        } else {
            $(this).attr('disabled','disabled');
            if($(this).val() == gpu) {
                $(this).prop('selected', false);
            }
        }
    });
    
    update_gpu_count();
}

function update_gpu_count() {
    let gpu = gpus_menu.val();
    
    if(gpu === '') {
        gpu_count.attr('disabled','disabled');
        gpu_count.removeAttr('required');
        gpu_count.val(0);
    }
    else {
        gpu_count.removeAttr('disabled');
        gpu_count.attr('required','required');
        gpu_count.val(1);
    }
}

reservations_menu.change(update_accounts);
accounts_menu.change(update_partitions);
partitions_menu.change(update_gpus);
gpus_menu.change(update_gpu_count);
update_accounts();